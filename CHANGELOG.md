# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.2] - 2024-03-12

Persistence is not futile.

### Changed

  - Internally, the current state is no longer stored as a reference to the current state object but as the key it is indexed with in the `states` object. This is so that the current state can be persisted in [JavaScript Database (JSDB)](https://codeberg.org/small-tech/jsdb) objects.

  - Internally, the `state` property is now guarded by an accessor.

  - Added a couple of `typedef`s to make the type information easier to read.

## [3.0.1] - 2024-03-12

You’re my type.

### Improved

  - Type information is now more accurate.
  - Turned on type checking via `@ts-check`.
  - Added more error checking; removed all TypeScript compiler warnings.

## [3.0.0] - 2024-03-11

Has-a not is-a proxy.

### Breaking changes

  - __The state class itself is no longer a proxy.__ It now uses a proxy to guard the states that are returned from it using the new `states` property. This means that instances/subclasses of `State` can now be persisted in [JavaScript Database (JSDB)](https://codeberg.org/small-tech/jsdb) objects (e.g., for use in [Kitten](https://codeberg.org/kitten/app) apps).

  - `stateInstance.now` is now `stateInstance.state`

  - Removed top-level `context` property. Instead, subclass the `State` class and define your own top-level properties.

  - Removed need to redundantly document the type of your states using JSDoc to get type safety and language intelligence. (Just subclass the `State` class as shown in the readme.)

## [2.0.0] - 2023-04-06

Free as in freedom.

### Added

  - Type information via JSDoc.

### Changed

  - Migrate repository from GitHub to Codeberg.
  - Change license from ISC to AGPL v3.
  - Drop Svelte support.

## [1.0.0] - 2021-06-13

Initial release.
