// @ts-check

/**
  Basic State class.

  Properties:

  - Only one state may be active at a time.
  - Attempting to access a non-existent state will throw a TypeError.
  - No new states can be added after initialisation (throws Error).

  Copyright ⓒ 2021-present Aral Balkan.
  Shared with ♥ by the Small Technology Foundation.

  Like this? Fund us!
  https://small-tech.org/fund-us

  @typedef {Object<string, any>} StateObject
  @typedef {Object<string, StateObject>} States
*/

export default class State {
  #setStateAccessorCalled = false

  /** @type {string} */
  _state

  /** @param {States} [states] */
  constructor ( states ) {
    if (states !== undefined) {
      this.states = states
    }
  }

  /**
    Returns the state object that matches the provided key
    or throws an error if either states have not been set yet
    or if the state object is not a member of the states object.

    @param {StateObject} state
  */
  findKeyForState (state) {
    if (this._states === undefined) {
      throw new Error(`States have not been set on ${this}`)
    }
    const key = Object.keys(this._states).find(key => {
      /** @ts-ignore Object cannot be undefined as we already checked outside closure. */
      return this._states[key] === state
    })
    if (key === undefined) {
      throw new Error(`The state you’re trying to set (${state}) does not exist on ${this}.`)
    }
    return key
  }

  /**
    Returns the state object (context) of the current state.

    @type {StateObject}
  */
  get state () {
    return this.states[this._state]
  }

  /**
    Sets the current state. This setter can only be called once and the expected use case
    is that it should only be called when a subclass is being instantiated.

    @param {StateObject} state
  */
  set state (state) {
    if (this.#setStateAccessorCalled) {
      throw new Error(`The state has already been set (state is a readonly property after first set)`)
    }
    const key = this.findKeyForState(state)
    this._state = key
    this.#setStateAccessorCalled = true
  }

  /**
    This returns a proxy to ensure that new states cannot be added once
    a state object has been created and that state updates can only take place
    via the set() method. In other words, it acts as a guard.

    @type {States}
  */
  get states () {
    if (this._states === undefined) {
      throw new Error(`States have not been set on ${this}`)
    }

    return /** @type {States} */ (new Proxy(this._states, {
      get: (_target, property, _receiver) => {
        /** @ts-ignore Object cannot be undefined as we already checked before returning proxy. */
        let value = this._states[String(property)]

        // Non-existent member access attempts throw an error. (We want
        // to fail fast on non-existent state look-ups.)
        if (value === undefined) {
          throw new TypeError(`Missing property on state: ${String(property)}`)
        }

        return value
      },

      set (_object, property, _value, _receiver) {
        // Do not allow properties to be directly set. State changes
        // must use the set() method exclusively.
        throw new Error(`Cannot directly set property (${String(property)}) on state. Please use the set() method instead.`)
      }
    }))
  }

  /** @param {States} states */
  set states (states) {
    if (this._states !== undefined) {
      throw new Error(`States have already been set. ${this._states}`)
    }
    this._states = states

    // The first state is the default.
    this._state = Object.keys(this._states)[0]
  }

  /**
    Is the passed state the current state?
      @param {StateObject} state
  */
  is (state) {
    return this.state === state
  }

  /**
    Change current state to passed state and update optional context (data) for that state.

    @param {StateObject} state
    @param {Object} [context]
  */
  set (state, context) {
    if (this._states === undefined) {
      throw new Error(`States have not been set on ${this}`)
    }

    const key = this.findKeyForState(state)

    // Only update the context if one is passed.
    if (context !== undefined) {
      this._states[key] = context
    }
    this._state = key
  }
}
