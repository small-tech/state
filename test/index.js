// @ts-check
import test from 'tape'
import State from '../State.js'

const fixtures = {
  state: () => new State({
    UNKNOWN: { id: 0 }, OK: { id: 1 }, NOT_OK: { id: 2 }
  })
}

/**
  Check whether candidate is Object instance.
  @param {any} candidate
*/
function isObject (candidate) {
  return candidate && candidate.constructor === Object
}

test('general', t => {
  t.plan(7)

  const state = fixtures.state()

  t.assert(state.states.UNKNOWN.id === 0 && state.states.OK.id === 1 && state.states.NOT_OK.id === 2, 'State is created.')
  t.assert(state.is(state.states.UNKNOWN) && !state.is(state.states.OK) && !state.is(state.states.NOT_OK), 'Default state is correctly set.')

  state.set(state.states.OK)
  t.assert(state.is(state.states.OK) && !state.is(state.states.UNKNOWN) && !state.is(state.states.NOT_OK), 'State is as expected after state change without context update.')
  t.assert(isObject(state.states.OK) && Object.keys(state.states.OK).length === 1 && state.states.OK.id === 1, 'Context is same as before after state change without context update.')

  const ERROR_MESSAGE = 'This is just not ok.'
  state.set(state.states.NOT_OK, { error: ERROR_MESSAGE })
  t.assert(state.is(state.states.NOT_OK) && !state.is(state.states.UNKNOWN) && !state.is(state.states.OK), 'State is as expected after state change with context update.')
  t.assert(isObject(state.states.NOT_OK) && Object.keys(state.states.NOT_OK).length === 1 && state.states.NOT_OK.error === ERROR_MESSAGE, 'Context is as expected after state change with context update.')

  state.set(state.states.OK)
  t.assert(state.state === state.states.OK && state.state !== state.states.UNKNOWN && state.state !== state.states.NOT_OK, 'State’s now property is set correctly.')
  
  t.end()
})

test('subclasses', t => {
  t.plan(1)

  class StatusState extends State {
    /** @param {string} name */
    constructor(name) {
      super()
      this.states = {
        UNKNOWN: { text: 'Unknown'},
        OK: { text: 'OK' },
        NOT_OK: { text: 'Not OK' }
      }
      this.state = this.states.UNKNOWN
      this.name = name
    }

    render () {
      const html = String.raw
      return html`<div>${this.name} Status: ${this.state.text}</div>`
    }
  }

  const status = new StatusState('Sync')
  status.set(status.states.OK)
  const div = status.render()

  t.equals(div, '<div>Sync Status: OK</div>', 'The rendered div is as expected.')

  t.end()
})

test('guards', t => {
  t.plan(10)

  const state = fixtures.state()
  const uninitialisedState = new State()

  t.throws(() => {
    state.states.DOES_NOT_EXIST
  }, 'Attempting to access non-existent state throws.')

  t.throws(() => {
    state.states.DOES_NOT_EXIST = {}
  }, 'Attempting to add a state after initialisation throws.')

  t.throws(() => {
    state.states.OK = { contextUpdated: true }
  }, 'Attempting to directly update the context of an existing state throws.')

  t.throws(() => {
    state.states = { OOPS: { id: 0 } }
  }, 'Attempting to set states again throws.')

  t.doesNotThrow(() => {
    state.state = state.states.OK
  }, 'First use of state setter does not throw.')

  t.throws(() => {
    state.state = state.states.NOT_OK
  }, 'Subsequent calls of state setter throw.')

  t.throws(() => {
    state.set({})
  }, 'Attempting to set unknown state throws.')
  
  t.throws(() => {
    uninitialisedState.states
  }, 'Attempting to access uninitialised states throws.')

  t.throws(() => {
    uninitialisedState.set({})
  }, 'Attempting to set state on uninitialised State instance throws (set method).')

  t.throws(() => {
    uninitialisedState.state = {}
  }, 'Attempting to set state on uninitialised State instance throws (setter).')

  t.end()
})
